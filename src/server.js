const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express();

app.get('/', (req, res) => {
    res.send('<a href="/zoro">Zoro page</a><br><a href="/hello-world">Hello world page</a>');
});

app.get('/zoro', (req, res) => {
    const pathToHtmlFile = path.resolve(__dirname, '../dist/zoro.html');
    const contentFromHtmlFile = fs.readFileSync(pathToHtmlFile, 'utf-8');
    res.send(contentFromHtmlFile);
});

app.get('/hello-world', (req, res) => {
    const pathToHtmlFile = path.resolve(__dirname, '../dist/hello-world.html');
    const contentFromHtmlFile = fs.readFileSync(pathToHtmlFile, 'utf-8');
    res.send(contentFromHtmlFile);
});

app.use('/static', express.static(path.resolve(__dirname, '../dist')));

app.listen(3000, () => {
    console.log('Server is running...')
});