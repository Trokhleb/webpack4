import addImage from './add-image.js';
import HelloWorldButton from './components/hello-world-button/hello-world-button.js';
import Heading from './components/heading/heading.js';
import BackButton from './components/back-button/back-button.js';
import 'bootstrap';
import './index.scss';
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

library.add(faSpinner);
dom.watch();

const helloWorldButton = new HelloWorldButton();
helloWorldButton.render();
const heading = new Heading();
heading.render('hello world');
addImage();
const backButton = new BackButton();
backButton.render();

if(process.env.NODE_ENV === 'production') {
    console.log('production mode')
}
if(process.env.NODE_ENV === 'development') {
    console.log('development mode')
}

