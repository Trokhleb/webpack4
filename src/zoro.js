import Heading from './components/heading/heading.js';
import ZoroImage from './components/zoro-image/zoro-image.js';
import BackButton from './components/back-button/back-button.js';
import 'bootstrap';
import './index.scss';

const heading = new Heading();
heading.render('zoro');
const zoroImage = new ZoroImage();
zoroImage.render();
const backButton = new BackButton();
backButton.render();