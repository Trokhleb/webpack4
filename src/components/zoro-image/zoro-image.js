import Zoro from './zoro-image.png';
import './zoro-image.scss';

class ZoroImage {
    render () {
        const img = document.createElement('img');
        img.src = Zoro;
        img.alt = 'Zoro';
        img.classList.add('zoro-image');
        const body = document.querySelector('body');
        body.appendChild(img);
    }
}

export default ZoroImage;