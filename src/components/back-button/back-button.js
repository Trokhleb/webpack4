import './back-button.scss'

class BackButton {
    render() {
        const backButton = document.createElement('a');
        backButton.href = '/';
        backButton.innerText = 'Back';
        backButton.classList.add('back-button');
        const body = document.querySelector('body');
        body.appendChild(backButton);
    }
}

export default BackButton;